import gleam/io
import solutions/lasagna.{
  alarm, expected_minutes_in_oven, preparation_time_in_minutes,
  remaining_minutes_in_oven, total_time_in_minutes,
}

pub fn main() {
  let emno = expected_minutes_in_oven()
  let rmio = remaining_minutes_in_oven(30)
  let nol = preparation_time_in_minutes(3)
  let ttim = total_time_in_minutes(3, 30)
  let alr = alarm()
  io.debug(emno)
  io.debug(rmio)
  io.debug(nol)
  io.debug(ttim)
  io.println(alr)
}
