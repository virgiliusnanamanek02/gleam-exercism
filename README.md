# exercism

[![Package Version](https://img.shields.io/hexpm/v/exercism)](https://hex.pm/packages/exercism)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/exercism/)

```sh
gleam add exercism
```
```gleam
import exercism

pub fn main() {
  // TODO: An example of the project in use
}
```

Further documentation can be found at <https://hexdocs.pm/exercism>.

## Development

```sh
gleam run   # Run the project
gleam test  # Run the tests
gleam shell # Run an Erlang shell
```
